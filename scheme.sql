
drop table if exists Korisnik;
create table Korisnik (
  korisnikId integer primary key autoincrement,
  username text(20) unique not null,
  password text(20) not null,
  ime text(20) not null,
  prezime text(20) not null,
  adresa text(20) not null,
  rokuplate text(20),
  aktivan integer(2) default 0,
  uloga text(10) not null,
  autorid integer references autor(autorid)
);

drop table if exists KorisnikAkcija;
create table KorisnikAkcija (
  akcijaId integer primary key autoincrement,
  korisnikId integer references korisnik(korisnikId),
  datum text(15) not null,
  opis text(50) not null
);

drop table if exists Autor;
create table Autor (
  AutorId integer primary key autoincrement,
  Ime text(20) not null,
  Prezime text(20) not null,
  korisnikid integer references korisnik(korisnikid)
);

drop table if exists AutoriranjeZnanstvenogRada;
create table AutoriranjeZnanstvenogRada (
  autorID integer references Autor(autorid),
  znanstveniRadId integer references ZnanstveniRad(ZnanstveniRadId),
  primary key (autorid, ZnanstveniRadId)
);

drop table if exists Eksperiment;
create table Eksperiment (
  Eksperimentid integer primary key autoincrement,
  naziv text(50) not null,
  vrijemeod text(15) not null,
  vrijemedo text(15) not null,
  verificiran integer default 0,
  razvojnoOkruzenjeid integer references razvojnookruzenje(razvojnookruzenjeid),
  alatid integer references alat(alatid),
  platformaid integer references platforma(platrofmaid)
);

drop table if exists AutoriranjeEksperimenta;
create table AutoriranjeEksperimenta (
  autorId integer references Autor(autorId),
  eksperimentId integer references Eksperiment(eksperimentId),
  primary key (autorId, eksperimentId)
);

drop table if exists Ocjena;
create table Ocjena (
  OcjenaId integer primary key autoincrement,
  vrijednost real not null,
  komentar text(200),
  eksperimentId integer references eksperiment(eksperimentid),
  korisnikId integer references Korisnik(korisnikId)
);

drop table if exists ZnanstveniSkup;
create table ZnanstveniSkup (
  ZnanstveniSkupId integer primary key autoincrement,
  naziv text(30) not null,
  mjesto text(50) not null,
  datumod text(15) not null,
  datumdo text(15) not null,
  webstranica text(30) not null
);

drop table if exists ZnanstveniRad;
create table ZnanstveniRad (
  ZnanstveniRadId integer primary key autoincrement,
  naslov text(30) not null,
  sazetak text(500) not null,
  verificiran integer default 0,
  znanstveniSkupId integer references ZnanstveniSkup(ZnanstveniSkupId),
  casopisId integer references Casopis(CasopisId)
);

drop table if exists KljucneRijeci;
create table KljucneRijeci (
  kljucnaRijec text(50) not null,
  znanstveniRadId integer references ZnanstveniRad(znanstveniRadId),
  primary key (kljucnaRijec, znanstveniRadId)
);

drop table if exists Promjena;
create table Promjena (
  promjenaId integer primary key autoincrement,
  tip text(20),
  id integer
);

drop table if exists Prigovor;
create table Prigovor (
  prigovorId integer primary key autoincrement,
  tip text(20),
  targetid integer,
  autorid integer,
  tekst text(500)
);

drop table if exists Casopis;
create table Casopis (
  CasopisId integer primary key autoincrement,
  naziv text(30) not null,
  godina integer not null,
  rednibroj integer not null,
  webstranica text(30) not null
);

drop table if exists DodatniParametar;
create table DodatniParametar (
  DodatniParametarId integer primary key autoincrement,
  imeParametra text(30) not null,
  sadrzaj text(300),
  eksperimentId integer references Eksperiment(eksperimentId)
);

drop table if exists Rezultat;
create table Rezultat (
  RezultatId integer primary key autoincrement,
  imeRezultata text(30) not null,
  vrijednost real not null,
  jedinica string(10) not null,
  eksperimentId integer references Eksperiment(eksperimentId)
);

drop table if exists Platforma;
create table Platforma (
  platformaId integer primary key autoincrement,
  tip text(30) not null,
  naziv text(30) not null,
  skraceniNaziv text(30) not null,
  inacica text(30) not null,
  webStranica text(30),
  datasheet text(30),
  cijena real not null
);

drop table if exists RazvojnoOkruzenje;
create table RazvojnoOkruzenje (
  razvojnoOkruzenjeId integer primary key autoincrement,
  tip text(30) not null,
  naziv text(30) not null,
  skraceniNaziv text(30) not null,
  inacica text(30) not null,
  cijena real not null,
  webstranica text(30)
);

drop table if exists Alat;
create table Alat(
  alatId integer primary key autoincrement,
  tip text(30) not null,
  naziv text(30) not null,
  skraceniNaziv text(30) not null,
  inacica real not null,
  cijena real not null,
  webstranica text(30)
);

drop table if exists IspitniPrimjer;
create table IspitniPrimjer (
  IspitniPrimjerId integer primary key autoincrement,
  nazivPrimjera text(300),
  ulazniPodaci text(30) not null
);

drop table if exists IspitniPrimjerEksperiment;
create table IspitniPrimjerEksperiment (
  ispitniprimjerid integer references ispitniprimjer(ispitniprimjerid),
  eksperimentId integer references eksperiment(eksperimentid),
  primary key(ispitniprimjerid, eksperimentid)
);

drop table if exists Sklopovlje;
create table Sklopovlje (
  SklopovljeId integer primary key autoincrement,
  naziv text(30) not null,
  karakteristika text(30) not null,
  platformaId integer references Platforma(platformaId)
);

drop table if exists Uredaj;
create table Uredaj (
  UredajId integer primary key autoincrement,
  naziv text(30) not null,
  webstranica text(30) not null,
  dashsheet test(30)
);

drop table if exists Element;
create table Element (
  sklopovljeId integer references Sklopovlje(SklopovljeId),
  uredajId integer references Element(ElementId),
  primary key (sklopovljeId, UredajId)
);

-- Korisnik 1
insert into Korisnik
(username, password, ime,
  prezime, adresa, rokuplate,
  aktivan, uloga, autorid)
values (
  "buda",
  "asdf",
  "Ivan",
  "Mandura",
  "Milutina Baraca 16",
  "12.3.1993.",
  1,
  "admin",
  Null
);

-- Korisnik 2
insert into Korisnik (username, password, ime,
  prezime, adresa, rokuplate,
  aktivan, uloga, autorid)
values (
  "ikatanic",
  "asdf",
  "Ivan",
  "Katanic",
  "Pozega",
  "20.3.1993.",
  1,
  "reg-user",
  Null
);

-- Korisnik 3
insert into Korisnik
(username, password, ime,
  prezime, adresa, rokuplate,
  aktivan, uloga, autorid)
values (
  "buba",
  "jklc",
  "Buba",
  "Trkulja",
  "Sloba",
  "14.5.2014.",
  0,
  "reg-user",
  Null
);

-- Korisnik 4
insert into Korisnik
(username, password, ime,
  prezime, adresa, rokuplate,
  aktivan, uloga, autorid)
values (
  "gustav",
  "duhovi",
  "Gustav",
  "Matula",
  "Zagreb",
  "14.5.2014.",
  1,
  "reg-user",
  NULL
);

-- Korisnik 5
insert into Korisnik
(username, password, ime,
  prezime, adresa, rokuplate,
  aktivan, uloga, autorid)
values (
  "imih",
  "jklc",
  "Iva",
  "Miholić",
  "Zlatar",
  "14.5.2020.",
  1,
  "reg-user",
  NULL
);

-- Korisnik 6
insert into Korisnik
(username, password, ime,
  prezime, adresa, rokuplate,
  aktivan, uloga, autorid)
values (
  "gljiva",
  "jklc",
  "Ivan",
  "Gljiva",
  "-",
  "14.5.2020.",
  0,
  "reg-user",
  NULL
);

-- Korisnik 7
insert into Korisnik
(username, password, ime,
  prezime, adresa, rokuplate,
  aktivan, uloga, autorid)
values (
  "dario",
  "jklc",
  "Dario",
  "Pazin",
  "-",
  "14.5.2020.",
  1,
  "reg-user",
  NULL
);

-- Korisnik 8
insert into Korisnik
(username, password, ime,
  prezime, adresa, rokuplate,
  aktivan, uloga, autorid)
values (
  "expert",
  "asdf",
  "Kljukica",
  "Kvakić",
  "Nigdjezemska",
  "14.5.2520.",
  1,
  "expert",
  NULL
);

-- Autor 1
insert into Autor
(Ime, Prezime)
values("Hrvoje", "Horvat");

-- Autor 2
insert into Autor
(Ime, Prezime)
values("Ivica", "Ivić");

-- Autor 3
insert into Autor
(Ime, Prezime, korisnikId)
values("Ivan", "Mandura", 1);

-- Autor 4
insert into Autor
(Ime, Prezime, korisnikId)
values("Ivan", "Katanić", 2);

-- Autor 5
insert into Autor(Ime, Prezime, korisnikId)
values("Sandra", "Trkulja", 3);

-- Autor 6
insert into Autor(Ime, Prezime, korisnikId)
values("Iva", "Miholić", 5);

-- Autor 7
insert into Autor(Ime, Prezime, korisnikId)
values("Ivan", "Gljiva", 6);

-- Autor 8
insert into Autor(Ime, Prezime, korisnikId)
values("Gustav", "Matula", 4);

-- Autor 9
insert into Autor(Ime, Prezime, korisnikId)
values("Dario", "Pazin", 7);

-- Autor 10
insert into Autor(Ime, Prezime, korisnikId)
values("Kljukica", "Kvakić", 8);

-- AutoriranjeZnanstvenogRada 1
insert  into AutoriranjeZnanstvenogRada(autorId, znanstveniRadId)
 values(1, 1);

insert  into AutoriranjeZnanstvenogRada(autorId, znanstveniRadId)
 values(2, 2);

insert  into AutoriranjeZnanstvenogRada(autorId, znanstveniRadId)
 values(3, 3);

insert  into AutoriranjeZnanstvenogRada(autorId, znanstveniRadId)
 values(4, 4);

insert  into AutoriranjeZnanstvenogRada(autorId, znanstveniRadId)
 values(5, 5);

insert  into AutoriranjeZnanstvenogRada(autorId, znanstveniRadId)
 values(6, 6);

-- Ocjena
insert into Ocjena
 (vrijednost, komentar, eksperimentId, korisnikId)
 values (3, "nije dobro", 3, 1);

insert into Ocjena
 (vrijednost, komentar, eksperimentId, korisnikId)
values(5, "supach", 1, 3);

insert into Ocjena
 (vrijednost, komentar, eksperimentId, korisnikId)
 values(2, "kul", 1, 2);

--  Znanstveni skup
insert into ZnanstveniSkup
 (naziv, mjesto, datumod, datumdo, webstranica)
values ("Skup1", "Mjesto1", "13.3.2013.", "15.3.2013.", "nedostaje");

insert into ZnanstveniSkup
 (naziv, mjesto, datumod, datumdo, webstranica)
values ("Skup2", "Mjesto1", "13.3.2013.", "15.3.2013.", "nedostaje");

insert into ZnanstveniSkup
 (naziv, mjesto, datumod, datumdo, webstranica)
values ("Skup3", "Mjesto3", "13.5.2013.", "15.6.2013.", "nedostaje");

-- Znanstveni Rad
insert into ZnanstveniRad
 (naslov, sazetak, znanstveniSkupId)
values ("ZRad1", "Sazetak sazetak sazetak", 1);
insert into ZnanstveniRad
 (naslov, sazetak, znanstveniSkupId)
values ("ZRad2", "Sazetak sazetak sazetak", 1);

insert into ZnanstveniRad
 (naslov, sazetak, znanstveniSkupId)
values ("ZRad3", "Sazetak sazetak sazetak", 2);

insert into ZnanstveniRad
 (naslov, sazetak, casopisId)
values("Zrad4", "Sazetak sazetak sazetak", 1);
insert into ZnanstveniRad
 (naslov, sazetak, casopisId)
values("Zrad5", "Sazetak sazetak sazetak", 2);
insert into ZnanstveniRad
 (naslov, sazetak, casopisId)
values("Zrad6", "Sazetak sazetak sazetak",1 );

-- KljucneRijeci
insert into KljucneRijeci
 (kljucnaRijec, znanstveniRadId)
values ("test", 1);

insert into KljucneRijeci
 (kljucnaRijec, znanstveniRadId)
values ("test", 2);

insert into KljucneRijeci
 (kljucnaRijec, znanstveniRadId)
values ("test", 3);

insert into KljucneRijeci
 (kljucnaRijec, znanstveniRadId)
values ("test", 4);

insert into KljucneRijeci
 (kljucnaRijec, znanstveniRadId)
values ("biologija", 5);

insert into KljucneRijeci
 (kljucnaRijec, znanstveniRadId)
values ("fizika", 6);

insert into KljucneRijeci
 (kljucnaRijec, znanstveniRadId)
values ("cs", 1);

-- Casopis
insert into Casopis
 (naziv, godina, rednibroj, webstranica)
values ("Caspopis1", 1993, 3, "casopis1.hr");

insert into Casopis
 (naziv, godina, rednibroj, webstranica)
values ("Caspopis2", 2013, 3, "-");

insert into Casopis
 (naziv, godina, rednibroj, webstranica)
values ("Caspopis3", 2003, 3, "-");

-- Platforma 1
insert into Platforma
(tip, naziv, skraceniNaziv, inacica, webStranica, cijena)
values ("Raspodijeljeni sustav", "HRGRID", "GRID1", "1.0", "www.heartresonance.com", 1000.00);

-- Platforma 2
insert into Platforma
(tip, naziv, skraceniNaziv, inacica, webStranica, cijena)
values ("Višeprocesorski sustav", "HR-NUMAchine", "NUMA1", "1.0",
  "en.wikipedia.org/wiki/NuMachine", 500.00);

-- Platforma 3
insert into Platforma
(tip, naziv, skraceniNaziv, inacica,  cijena)
values ("Poslužitelj", "Moj poslužitelj", "Server1", "1.0", 0);

-- Platforma 4
insert into Platforma
(tip, naziv, skraceniNaziv, inacica, webStranica, cijena)
values ("Računalo", "Moje računalo", "Q9400",  "1.1",
  "http://ark.intel.com/products/35365/Intel-Core2-Quad-Processor-Q9400-6M-Cache-2_66-GHz-1333-MHz-FSB",
  5000);

-- Platforma 5
insert into Platforma
(tip, naziv, skraceniNaziv, inacica, webStranica, cijena)
values ("Razvojna ploča", "Xilinx ML506", "ML506", "1.1",
  "http://www.xilinx.com/products/boards/ml506/reference_designs.htm",
  300);

-- Razvojno okruzenje
insert into RazvojnoOkruzenje
(tip, naziv, skraceniNaziv, inacica, cijena)
values ("prog", "Eclipse", "IDE1", "Kepler", 0.00);

insert into RazvojnoOkruzenje
(tip, naziv, skraceniNaziv, inacica, cijena)
values ("prog", "Rstudio", "IDE2", "5", 0.00);

insert into RazvojnoOkruzenje
(tip, naziv, skraceniNaziv, inacica, cijena)
values ("prog", "Matlab", "IDE3", "13", 50.00);

-- Alat 1
insert into Alat
(tip, naziv, skraceniNaziv, inacica, cijena, webstranica)
values ("Tekstualni editor", "Vim", "Tool1", "7.4", 0.00, "www.vim.org");

-- Alat 2
insert into Alat
(tip, naziv, skraceniNaziv, inacica, cijena, webstranica)
values ("Tekstualni editor", "Emacs", "Tool2", "24.3", 0.00,
  "http://www.gnu.org/software/emacs/111");

-- Alat 3
insert into Alat
(tip, naziv, skraceniNaziv, inacica, cijena, webstranica)
values ("Code Hosting tool", "BitBucket", "Tool3", "-", 0.00,
  "https://www.atlassian.com/software/bitbucket");

-- Ispitni primjer
insert into IspitniPrimjer
 (nazivPrimjera, ulazniPodaci)
values ("ulaz1", "1 1 1\n");

insert into IspitniPrimjer
 (nazivPrimjera, ulazniPodaci)
values ("ulaz2", ";ulaz2\n 3 11 19 20 18 5\n");

insert into IspitniPrimjer
 (nazivPrimjera, ulazniPodaci)
values ("ulaz3", "do re mi fa so la ti do");

-- Sklopovlje
insert into Sklopovlje
(naziv, karakteristika, platformaId)
values("Q9400", "Intel Core2 Quad Q9400", 4);

insert into Sklopovlje
(naziv, karakteristika, platformaId)
values("WinXP", "Windows XP", 4);

insert into Sklopovlje
(naziv, karakteristika, platformaId)
values("XC5VSX50T", "Virtex-5 FPGA", 5);


-- Uredaj
insert into Uredaj
 (naziv, webstranica)
values ("Uredaj1", "-");

insert into Uredaj
 (naziv, webstranica)
values ("Uredaj2", "-");

insert into Uredaj
 (naziv, webstranica)
values ("Uredaj3", "-");

insert into Uredaj
 (naziv, webstranica)
values ("Uredaj4", "-");

insert into Uredaj
 (naziv, webstranica)
values ("Uredaj5", "-");

-- ELement
insert into Element(sklopovljeid, uredajid)
values(1, 1);

insert into Element(sklopovljeid, uredajid)
values(2, 2);

insert into Element(sklopovljeid, uredajid)
values(3, 3);

insert into Element(sklopovljeid, uredajid)
values(1, 4);

insert into Element(sklopovljeid, uredajid)
values(2, 5);


--Eksperiment 1
insert into Eksperiment
        (naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
        values("Test1","08.01.2012. 16:45","20.04.2013. 15:55",3,1,2);

insert into AutoriranjeEksperimenta
        (autorId, eksperimentId)
values(4,1);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par1","30",1);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz3",1);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par3","option2",1);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1", 257.517486816,"T",1);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2", 786.345305464,"T",1);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3", 2567.31835286,"T",1);

insert into IspitniPrimjerEksperiment
        (ispitniprimjerid, eksperimentid)
values(3,1);

--Eksperiment 2
insert into Eksperiment
        (naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
        values("Test2","11.11.2012. 06:24","14.10.2013. 12:09",2,3,1);

insert into AutoriranjeEksperimenta
        (autorId, eksperimentId)
values(1,2);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par1","19",2);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz1",2);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par3","option2",2);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1", 555.217834433,"km",2);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2", 567.81001162,"km",2);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3", 4861.68459622,"km",2);

insert into IspitniPrimjerEksperiment
        (ispitniprimjerid, eksperimentid)
values(1,2);


--Eksperiment 3
insert into Eksperiment
        (naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
        values("Test3","11.08.2012. 14:58","23.02.2013. 20:59",2,2,1);

insert into AutoriranjeEksperimenta
        (autorId, eksperimentId)
values(2,3);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par1","5",3);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz4",3);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par3","option3",3);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1", 795.784233293,"C",3);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2", 914.792206545,"C",3);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3", 620.338227308,"C",3);

insert into IspitniPrimjerEksperiment
        (ispitniprimjerid, eksperimentid)
values(4,3);

--Eksperiment 4
insert into Eksperiment
        (naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
        values("Test4","30.09.2012. 09:36","07.02.2013. 22:51",3,1,3);

insert into AutoriranjeEksperimenta
        (autorId, eksperimentId)
values(4,4);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par1","31",4);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz2",4);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par3","option2",4);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1", 2861.44280051,"km",4);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2", 16.4621855824,"km",4);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3", 248.292265395,"km",4);

insert into IspitniPrimjerEksperiment
        (ispitniprimjerid, eksperimentid)
values(2,4);

--Eksperiment 5
insert into Eksperiment
        (naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
        values("Test5","12.03.2012. 06:29","04.02.2013. 15:10",2,3,1);

insert into AutoriranjeEksperimenta
        (autorId, eksperimentId)
values(8,5);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par1","31",5);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz2",5);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par3","option4",5);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1", 113.700820543,"km",5);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2", 7554.10822963,"km",5);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3", 3377.38635355,"km",5);

insert into IspitniPrimjerEksperiment
        (ispitniprimjerid, eksperimentid)
values(2,5);

--Eksperiment 6
insert into Eksperiment
        (naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
        values("Test6","18.11.2012. 15:05","20.05.2013. 19:52",2,2,2);

insert into AutoriranjeEksperimenta
        (autorId, eksperimentId)
values(2,6);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par1","13",6);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz5",6);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par3","option3",6);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1", 263.933861742,"C",6);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2", 951.562403342,"C",6);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3", 2165.27790066,"C",6);

insert into IspitniPrimjerEksperiment
        (ispitniprimjerid, eksperimentid)
values(5,6);

--Eksperiment 7
insert into Eksperiment
        (naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
        values("Test7","26.09.2012. 11:44","30.08.2013. 14:16",3,3,2);

insert into AutoriranjeEksperimenta
        (autorId, eksperimentId)
values(5,7);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par1","44",7);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz5",7);

insert into DodatniParametar
        (imeParametra, sadrzaj, eksperimentId)
values("par3","option3",7);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1", 1092.5432198,"mA",7);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2", 1811.61722151,"mA",7);

insert into Rezultat
        (imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3", 2730.25682413,"mA",7);

insert into IspitniPrimjerEksperiment
        (ispitniprimjerid, eksperimentid)
values(5,7);


--Eksperiment 8
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test8","12.10.2012. 13:26","15.11.2013. 07:36",3,3,2);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(5,8);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","33",8);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz5",8);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option2",8);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",1696.47382245,"K",8);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",2022.57425032,"K",8);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",150.68938611,"K",8);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(5,8);

--Eksperiment 9
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test9","12.01.2012. 18:54","21.07.2013. 19:01",2,3,1);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(9,9);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","28",9);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz4",9);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option5",9);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",4002.56156896,"cm",9);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",573.307641766,"cm",9);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",6519.15633294,"cm",9);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(4,9);

--Eksperiment 10
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test10","11.10.2012. 00:07","10.11.2013. 01:51",3,2,1);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(6,10);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","30",10);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz4",10);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option5",10);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",2006.41389038,"J",10);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",25.0273298335,"J",10);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",4795.14200893,"J",10);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(4,10);

--Eksperiment 11
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test11","13.01.2012. 00:04","07.07.2013. 08:46",3,3,2);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(8,11);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","16",11);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz5",11);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option3",11);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",4231.96966619,"dm2",11);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",1629.42112277,"dm2",11);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",307.680334395,"dm2",11);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(5,11);

--Eksperiment 12
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test12","04.04.2012. 19:47","22.06.2013. 10:26",3,2,2);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(9,12);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","48",12);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz5",12);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option4",12);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",243.159124645,"mA",12);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",47.773930415,"mA",12);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",804.215517037,"mA",12);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(5,12);

--Eksperiment 13
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test13","20.09.2012. 19:02","13.02.2013. 06:49",2,3,1);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(9,13);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","15",13);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz2",13);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option2",13);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",6901.07045595,"dm2",13);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",149.300391293,"dm2",13);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",1617.16803003,"dm2",13);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(2,13);

--Eksperiment 14
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test14","15.09.2012. 20:20","31.07.2013. 21:59",2,3,2);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(7,14);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","18",14);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz4",14);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option4",14);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",4159.6432563,"K",14);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",2961.90233241,"K",14);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",1065.7866716,"K",14);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(4,14);

--Eksperiment 15
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test15","03.11.2012. 17:17","06.10.2013. 15:17",3,1,1);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(7,15);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","18",15);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz4",15);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option5",15);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",1608.66481851,"pA",15);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",1492.37791386,"pA",15);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",28.9813868547,"pA",15);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(4,15);

--Eksperiment 16
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test16","13.06.2012. 03:13","27.12.2013. 00:44",3,3,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(7,16);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","27",16);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz4",16);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option2",16);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",587.257396378,"cm",16);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",983.647495638,"cm",16);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",737.521169477,"cm",16);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(4,16);

--Eksperiment 17
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test17","28.09.2012. 16:28","04.04.2013. 03:15",1,1,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(7,17);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","4",17);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz1",17);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option2",17);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",185.087205741,"K",17);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",731.307857449,"K",17);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",3782.99543696,"K",17);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(1,17);

--Eksperiment 18
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test18","01.12.2012. 07:27","21.03.2013. 07:34",2,3,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(6,18);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","48",18);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz1",18);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option4",18);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",2696.82160966,"km",18);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",661.578617992,"km",18);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",1203.12733357,"km",18);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(1,18);

--Eksperiment 19
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test19","31.10.2012. 14:04","25.11.2013. 12:39",2,3,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(1,19);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","37",19);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz1",19);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option2",19);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",4569.18693635,"K",19);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",7234.43929335,"K",19);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",1157.26114578,"K",19);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(1,19);

--Eksperiment 20
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test20","22.03.2012. 20:29","29.03.2013. 18:08",1,1,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(1,20);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","47",20);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz1",20);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option1",20);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",5219.12504755,"pA",20);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",1476.16261128,"pA",20);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",18.1688768858,"pA",20);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(1,20);

--Eksperiment 21
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test21","17.02.2012. 14:38","01.07.2013. 09:43",3,2,1);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(5,21);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","18",21);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz4",21);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option3",21);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",136.595609414,"J",21);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",1218.7829906,"J",21);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",4901.25746856,"J",21);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(4,21);

--Eksperiment 22
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test22","07.05.2012. 09:31","09.07.2013. 15:06",2,1,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(7,22);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","36",22);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz5",22);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option1",22);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",1769.15648395,"T",22);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",4499.14606434,"T",22);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",256.380681036,"T",22);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(5,22);

--Eksperiment 23
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test23","21.09.2012. 02:00","16.06.2013. 19:34",2,1,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(1,23);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","40",23);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz2",23);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option5",23);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",2994.32943398,"T",23);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",4251.79870863,"T",23);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",96.5822212193,"T",23);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(2,23);

--Eksperiment 24
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test24","17.12.2012. 11:26","22.04.2013. 00:09",1,3,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(9,24);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","13",24);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz4",24);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option3",24);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",4309.29053999,"dm2",24);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",2506.23733146,"dm2",24);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",156.066829807,"dm2",24);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(4,24);

--Eksperiment 25
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test25","24.10.2012. 07:37","12.08.2013. 10:41",3,1,2);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(9,25);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","8",25);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz2",25);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option4",25);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",207.869526968,"km",25);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",7721.83588843,"km",25);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",83.7950567318,"km",25);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(2,25);

--Eksperiment 26
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test26","21.09.2012. 02:11","30.07.2013. 22:49",1,1,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(9,26);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","40",26);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz1",26);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option4",26);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",4873.73274424,"mW",26);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",675.904194768,"mW",26);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",1540.17049252,"mW",26);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(1,26);

--Eksperiment 27
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test27","14.01.2012. 23:53","21.05.2013. 23:29",2,1,2);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(3,27);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","46",27);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz5",27);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option2",27);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",2919.84874215,"cm",27);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",2233.84049992,"cm",27);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",2402.15509061,"cm",27);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(5,27);

--Eksperiment 28
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test28","14.04.2012. 08:28","20.01.2013. 17:21",3,1,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(1,28);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","26",28);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz5",28);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option1",28);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",8112.13930642,"mA",28);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",2367.44710405,"mA",28);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",201.035306644,"mA",28);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(5,28);

--Eksperiment 29
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test29","14.12.2012. 16:25","01.04.2013. 06:26",1,1,1);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(5,29);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","8",29);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz3",29);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option2",29);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",422.379018278,"mW",29);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",3238.33855745,"mW",29);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",7640.02811469,"mW",29);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(3,29);

--Eksperiment 30
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test30","08.07.2012. 13:05","21.04.2013. 07:33",3,1,2);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(9,30);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","38",30);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz2",30);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option3",30);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",3278.13321187,"km",30);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",4417.44499603,"km",30);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",1387.54804255,"km",30);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(2,30);

--Eksperiment 31
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test31","26.05.2012. 08:24","24.07.2013. 14:44",3,2,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(3,31);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","42",31);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz1",31);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option3",31);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",5997.29420849,"dm2",31);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",1148.80757589,"dm2",31);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",30.8428874652,"dm2",31);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(1,31);

--Eksperiment 32
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test32","06.04.2012. 15:31","18.08.2013. 06:11",2,1,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(4,32);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","40",32);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz4",32);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option5",32);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",2964.56001752,"K",32);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",860.997801991,"K",32);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",3359.81489185,"K",32);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(4,32);

--Eksperiment 33
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test33","09.02.2012. 19:09","09.02.2013. 00:42",1,2,1);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(9,33);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","11",33);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz5",33);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option4",33);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",600.622065085,"T",33);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",5282.423516,"T",33);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",5327.75964032,"T",33);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(5,33);

--Eksperiment 34
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test34","09.06.2012. 18:46","29.11.2013. 23:01",1,3,1);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(7,34);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","46",34);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz1",34);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option4",34);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",2410.95965716,"T",34);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",3028.37605836,"T",34);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",144.586461305,"T",34);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(1,34);

--Eksperiment 35
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test35","03.05.2012. 21:31","12.09.2013. 14:51",3,2,2);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(4,35);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","39",35);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz2",35);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option5",35);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",7177.91330466,"km",35);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",6389.88609215,"km",35);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",1494.81531792,"km",35);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(2,35);

--Eksperiment 36
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test36","15.08.2012. 05:11","15.03.2013. 13:32",3,1,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(2,36);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","6",36);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz5",36);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option2",36);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",6661.16154723,"J",36);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",3622.34110729,"J",36);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",1729.51945424,"J",36);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(5,36);

--Eksperiment 37
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test37","21.10.2012. 23:53","20.12.2013. 13:19",1,2,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(7,37);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","10",37);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz1",37);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option1",37);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",3239.24146707,"cm",37);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",2015.14462803,"cm",37);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",390.082969627,"cm",37);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(1,37);

--Eksperiment 38
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test38","13.08.2012. 18:35","25.02.2013. 12:08",3,2,1);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(8,38);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","8",38);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz3",38);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option4",38);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",340.029706469,"dm2",38);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",4112.34987535,"dm2",38);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",68.7982257626,"dm2",38);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(3,38);

--Eksperiment 39
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test39","16.12.2012. 23:27","11.02.2013. 08:00",2,3,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(4,39);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","16",39);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz4",39);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option5",39);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",6246.7760403,"mW",39);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",911.757971367,"mW",39);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",605.854479229,"mW",39);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(4,39);

--Eksperiment 40
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test40","27.04.2012. 13:22","10.10.2013. 19:15",2,2,1);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(8,40);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","21",40);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz2",40);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option1",40);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",1381.65951269,"T",40);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",253.230831224,"T",40);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",1920.56474654,"T",40);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(2,40);

--Eksperiment 41
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test41","04.01.2012. 05:14","05.10.2013. 01:49",1,2,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(4,41);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","13",41);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz4",41);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option4",41);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",1501.28434893,"K",41);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",1494.9072001,"K",41);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",686.609956946,"K",41);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(4,41);

--Eksperiment 42
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test42","09.07.2012. 06:21","31.10.2013. 06:26",2,1,2);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(1,42);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","32",42);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz3",42);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option2",42);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",2113.72065788,"mW",42);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",187.446204351,"mW",42);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",7273.95247475,"mW",42);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(3,42);

--Eksperiment 43
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test43","09.11.2012. 22:40","25.05.2013. 04:43",2,3,2);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(6,43);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","41",43);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz2",43);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option1",43);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",605.524890458,"mW",43);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",1436.27391242,"mW",43);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",282.774978879,"mW",43);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(2,43);

--Eksperiment 44
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test44","02.03.2012. 16:34","31.01.2013. 04:16",3,2,2);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(9,44);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","22",44);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz4",44);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option2",44);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",1437.87091128,"mW",44);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",3623.40735513,"mW",44);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",960.42545701,"mW",44);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(4,44);

--Eksperiment 45
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test45","20.03.2012. 11:31","22.10.2013. 13:59",1,3,1);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(2,45);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","40",45);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz4",45);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option5",45);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",8350.3630936,"C",45);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",549.886902193,"C",45);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",7.41887700171,"C",45);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(4,45);

--Eksperiment 46
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test46","02.04.2012. 00:15","28.10.2013. 08:37",2,1,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(4,46);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","29",46);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz4",46);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option5",46);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",1.40276078507,"km",46);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",1462.00538414,"km",46);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",3676.5263422,"km",46);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(4,46);

--Eksperiment 47
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test47","27.04.2012. 10:40","02.02.2013. 03:57",2,3,3);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(2,47);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","31",47);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz5",47);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option1",47);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",3.6160623418,"dm2",47);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",1286.74885769,"dm2",47);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",80.0539002529,"dm2",47);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(5,47);

--Eksperiment 48
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test48","11.12.2012. 23:23","12.04.2013. 10:18",3,3,2);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(6,48);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","5",48);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz5",48);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option3",48);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",1225.41810894,"pA",48);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",2702.59177976,"pA",48);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",343.407294559,"pA",48);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(5,48);

--Eksperiment 49
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test49","24.06.2012. 10:41","07.01.2013. 16:14",2,2,1);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(9,49);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","32",49);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz5",49);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option5",49);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",9100.21774564,"dm2",49);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",4315.38717701,"dm2",49);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",2631.90995047,"dm2",49);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(5,49);

--Eksperiment 50
insert into  Eksperiment
	(naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeid, alatid, platformaid)
	values("Test50","16.04.2012. 21:12","02.06.2013. 19:12",2,2,2);

insert into  AutoriranjeEksperimenta
	(autorId, eksperimentId)
values(8,50);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par1","50",50);

insert into  DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par2","ulaz2",50);

insert into DodatniParametar
	(imeParametra, sadrzaj, eksperimentId)
values("par3","option1",50);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val1",1268.0757479,"C",50);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val2",245.682834424,"C",50);

insert into  Rezultat
	(imeRezultata, vrijednost, jedinica, eksperimentId)
values("val3",4919.65425875,"C",50);

insert into   IspitniPrimjerEksperiment
	(ispitniprimjerid, eksperimentid)
values(2,50);

INSERT INTO Prigovor (tip, targetid, autorid, tekst) VALUES ('experiment', 1, 3, 'tekst');
INSERT INTO Prigovor (tip, targetid, autorid, tekst) VALUES ('experiment', 2, 2, 'tekst1');
INSERT INTO Prigovor (tip, targetid, autorid, tekst) VALUES ('experiment', 3, 1, 'tekst2');
