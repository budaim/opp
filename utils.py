
from flask_login import login_required
from flask import request, current_user

def role_required(role):
   def wrap(func):
      def wrapper(*args, **kwargs):
         if current_user.role >= role:
            func(*args, **kwargs)
      return wrapper
   return wrap

