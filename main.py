#!/usr/bin/python

import sqlite3 as sqlite
import os
import datetime

from flask import Flask, g, render_template, request, session, redirect, url_for, flash, make_response
from flask_login import  LoginManager, current_user, login_user, logout_user
from flask_login import login_required

app = Flask(__name__, static_folder='static', static_url_path='')

DATABASE = 'base.db'
DEBUG = True
SECRET_KEY = 'dev key'
USERNAME = 'admin'
PASSWORD = 'default'
DB_SCHEME = 'scheme.sql'
UPLOAD_FOLDER = 'static/data'

app.config.from_object(__name__)
app.config.from_envvar('MAIN_CONFIG', silent=True)

login_manager = LoginManager()
login_manager.init_app(app)


def connect_db():
   db = sqlite.connect(app.config['DATABASE'])
   return db


def init_db():
   with app.app_context():
      db = get_db()
      with app.open_resource(app.config['DB_SCHEME'], mode='r') as f:
         db.cursor().executescript(f.read())
      db.commit()


def get_db():
   if not hasattr(g, 'db'):
      g.db = connect_db()
   return g.db


@app.teardown_appcontext
def close_database(ex):
   if hasattr(g, 'db'):
      g.db.close()


def query_db(sql, *args):
   cur = get_db().cursor().execute(sql, args)
   res = cur.fetchall()
   cur.close()
   return res


def get_authors(eid):
   author_id = current_authorid()

   sql = '''
         select autorid from AutoriranjeEksperimenta
         where eksperimentId = ?
         '''

   autori = query_db(sql, eid)
   return [x for x, in autori]


def update_db(sql, *args):
   db = get_db()
   cur = db.execute(sql, args)
   db.commit()
   return cur.lastrowid


class User(object):
   def __init__(self, tup):
      ((self.userid, self.username, self.password),
            (self.role, self.active)) = (tup[0:3], tup[-2:])

   def is_authenticated(self):
      return True

   def is_active(self):
      return True

   def is_anonymous(self):
      return False

   def get_id(self):
      return self.username

   def get_role():
      return self.role


@login_manager.user_loader
def load_user(userid):
   sql = 'select * from Korisnik where username = ?'
   res = query_db(sql, userid)
   return User(res[0]) 


@app.route('/login', methods=['GET', 'POST'])
def login(back_page='index'):
   if request.method == 'POST':
      try:
         user = load_user(unicode(request.form['Username']))
      except:
         user = None
      password = unicode(request.form['Password'])
      if user is not None and password == user.password:
         login_user(user)
         return redirect(url_for(back_page))
      flash('Krivi podaci')
   return render_template('login.html')


@app.route('/logout')
@login_required
def logout():
   logout_user()
   return redirect(url_for('login'))


@app.route('/register', methods=['GET', 'POST'])
def register():
   db = get_db().cursor()
   errors = []

   if request.method == 'POST':
      values = [
            request.form['Username'], request.form['Password'],
            request.form['FirstName'], request.form['LastName'],
            request.form['Address']]
      sql = '''
            insert into Korisnik
            (username, password, ime, prezime, adresa,  uloga)
            values (?, ?, ?, ?, ?, "reg-user") '''
      uid = update_db(sql, *values)
      
      values = [request.form['FirstName'], request.form['LastName'], uid]
      sql = '''
            insert into Autor
            (ime, prezime, korisnikId)
            values (?, ?, ?)
            '''
      update_db(sql, *values)

      return redirect(url_for('login'))

   return render_template('register.html')


def current_authorid():
   sql = 'select autorId from Autor where korisnikId = ?'
   authorId, = query_db(sql, current_user.userid)[0]
   return authorId


@app.route('/')
@app.route('/index')
def index():
   role = getattr(current_user, 'role', 'non-reg')
   if role == 'non-reg':
      sql = 'select znanstveniRadId, naslov from ZnanstveniRad'
      works = query_db(sql)
      return render_template('nonreg_index.html', works=works)
   elif role == 'admin':
      return redirect(url_for('sve_registracije'))
   else:
      sql = '''
            select zr.znanstveniRadId, naslov, a.ime, a.prezime
            from ZnanstveniRad as zr
            join AutoriranjeZnanstvenogRada as azr
               on azr.ZnanstveniRadid = zr.ZnanstveniRadid
            join Autor as a
               on a.autorid = azr.autorid'''

      works = query_db(sql)
      works = [(i, n, ime + ' ' + prezime) for i, n, ime, prezime in works]

      sql = '''
            select e.eksperimentId, naziv, a.ime, a.prezime
            from Eksperiment as e
            join AutoriranjeEksperimenta as ae
               on ae.eksperimentId = e.eksperimentId
            join Autor as a
               on a.autorid = ae.autorid'''

      expts = query_db(sql)
      expts = [(i, n, ime + ' ' + prezime) for i, n, ime, prezime in expts]

      return render_template('reg_index.html', works=works, expts=expts)


@app.route('/works/<uid>', methods=['GET', 'POST'])
def show_work(uid):
   if request.method == 'POST':
      flash('Rad je verificiran')
      build_update_query(
            "ZnanstveniRad",
            "ZnanstveniRadId",
            uid,
            {"verificiran": 1})

   sql = 'select * from ZnanstveniRad where ZnanstveniRadid=?'
   q = query_db(sql, uid)[0]
   _, title, summary, ver, skupId, casopisId = q

   sql = 'select naziv from Casopis where casopisId=?'
   res = query_db(sql, casopisId)
   casopis, = res[0] if res else (None,) 

   sql = 'select naziv from ZnanstveniSkup where znanstveniSkupId=?'
   res = query_db(sql, casopisId)
   skup, = res[0] if res else (None,)

   return render_template(
         'works.html', 
         title=title, 
         summary=summary,
         auth=not current_user.is_anonymous(),
         isSkup=not (skup is None),
         skup=skup,
         isCasopis=not (casopis is None),
         casopis=casopis,
         verificiran=ver,
         filename='work%s.txt' % str(uid)
   )


def get_elements():
   sql = 'select platformaid, naziv from platforma'
   platforme = query_db(sql)
   sql = 'select razvojnookruzenjeid, naziv from razvojnookruzenje'
   razvojnaOkruzenja = query_db(sql)
   sql = 'select alatid, naziv from alat'
   alati = query_db(sql)
   return platforme, razvojnaOkruzenja, alati


def query_factory(args):
   sql = ''' select e.eksperimentId, e.naziv
         from Eksperiment as e
         join AutoriranjeEksperimenta as ae
            on e.eksperimentid = ae.eksperimentid
         where 1 = 1 '''

   skip = 'rezultati'

   for key, value in args.iteritems():
      if key != skip and value.strip():
         sql += " and cast(%s as text) = '%s'" % (key, value)

   exps = query_db(sql)
   vals = [x.strip() for x in args.get(skip, '').split(' ') if x.strip()]

   sql = '''
         select e.eksperimentid, e.naziv, r.imeRezultata, r.jedinica, r.vrijednost
         from Eksperiment as e
         join Rezultat as r on r.eksperimentid = e.eksperimentid
         where (%s) and (%s)''' % (
               ' or '.join(['r.imeRezultata = ?'] * len(vals))
                  if vals else '1 = 1',
               ' or '.join(['e.eksperimentId = ?'] * len(exps))
                  if exps else '1 = 0')

   print sql
   exp_ids = [i for i, _ in exps]
   exps = query_db(sql, *(vals + exp_ids))

   return exps


@app.route('/search', methods=['GET'])
@login_required
def search():
   platforme, razvojnaOkruzenja, alati = get_elements()
   platforme = [(str(i), e) for i, e in platforme]
   razvojnaOkruzenja = [(str(i), e) for i, e in razvojnaOkruzenja]
   alati = [(str(i), e) for i, e in alati]

   sql = 'select autorid, ime, prezime from Autor'
   authors = query_db(sql)
   authors = [(str(a), i + ' ' + p) for a, i, p in authors]

   allowed = ['naziv', 'vrijemeod', 'vrijemedo', 'alatId', 'platformaid', 'razvojnoOkruzenjeId', 'autorid', 'rezultati']

   data = {}
   for a in allowed:
      if a in request.args:
         data[a] = request.args[a]

   expts = query_factory(data)

   if 'download' in request.args:
      vals = sorted(x[2] for x in expts)
      resp = ','.join(['Exp'] + vals) + '\n'

      exps = set((x[0], x[1]) for x in expts)

      for uid, name in exps:
         raw = [name]
         rval = {x[2]: (x[4], x[3]) for x in expts if x[0] == uid}

         for v in vals:
            w, j = rval.get(v, ('-', '-'))
            raw.append("%s (%s)" % (w, j))

         resp += ','.join(raw) + '\n'

      response = make_response(resp)
      response.headers['Content-Disposition'] = "attachment; filename=results.csv"

      return response

   return render_template('search.html',
         expts=expts,
         platforme=platforme,
         razvojnaOkruzenja=razvojnaOkruzenja,
         alati=alati,
         authors=authors,
         **data
   )


def get_experiment(uid):
   sql = 'select * from Eksperiment where eksperimentId = ?'
   data = query_db(sql, uid)
   if data:
      return data[0]
   return None


def get_experiment_platform(uid):
   sql = 'select p.* from Platforma as p JOIN Eksperiment AS e on e.platformaid = p.platformaId WHERE e.Eksperimentid = ?'
   data = query_db(sql, uid)
   if data:
      return data[0]
   return None


def get_experiment_tool(uid):
   sql = 'select a.* from Alat as a JOIN Eksperiment AS e on e.alatid = a.alatId WHERE e.Eksperimentid = ?'
   data = query_db(sql, uid)
   if data:
      return data[0]
   return None


def get_experiment_env(uid):
   sql = 'select a.* from RazvojnoOkruzenje as a JOIN Eksperiment AS e on e.razvojnoOkruzenjeId = a.razvojnoOkruzenjeId WHERE e.Eksperimentid = ?'
   data = query_db(sql, uid)
   if data:
      return data[0]
   return None


def get_results(uid):
   sql = 'select r.* from Rezultat r WHERE eksperimentid = ?'
   return query_db(sql, uid)


def add_result(experimentid, name, value, unit):
   data = { 'imeRezultata': name, 'vrijednost': value, 'jedinica': unit, 'eksperimentId': experimentid}
   return build_insert_query(table_name='Rezultat', fields=data)


@app.route('/experiments/<expr_id>', methods=['POST', 'GET'])
@login_required
def show_experiment(expr_id):
   if request.method == 'POST':
      flash('Rad je verificiran')
      build_update_query(
            "Eksperiment", 
            "eksperimentId", 
            expr_id, 
            {"verificiran": 1})

   sql = 'select * from Eksperiment where eksperimentId = ?'
   (eid, naziv, vrijemeod, vrijemedo,
    verificiran, razvojnoOkruzenjeId, alatId,
    platformaId) = query_db(sql, expr_id)[0]

   author_ids = get_authors(expr_id)

   author_id = current_authorid()
   is_author = (author_id in author_ids)

   sql = 'select naziv from Platforma where platformaid = ?'
   platforma, = query_db(sql, platformaId)[0]
   sql = 'select naziv from RazvojnoOkruzenje where razvojnoOkruzenjeid = ?'
   razvojnoOkruzenje, = query_db(sql, razvojnoOkruzenjeId)[0]
   sql = 'select naziv from Alat where alatId = ?'
   alat, = query_db(sql, alatId)[0]

   sql = '''
         select ime, prezime from Autor
         where ''' + ' or '.join(['autorid = ?'] * len(author_ids))

   autori = query_db(sql, *author_ids)
   autori = map(' '.join, autori)

   sql = '''
         select vrijednost, komentar from Ocjena
         where eksperimentId = ?
         '''

   ok = query_db(sql, expr_id)
   ocjene, komentari = zip(*ok) if ok else ([0], [])
   ocjene = [x for x in ocjene if x]

   prosjek = round(float(sum(ocjene)) / len(ocjene), 2) if ocjene else 0

   rezultati = get_results(expr_id)

   ocjene = [i/2. for i in range(21)]
   ocjene.reverse()

   get_comments_sql = 'select (a.ime || \' \' || a.prezime), o.vrijednost, o.komentar from Ocjena o JOIN Autor a ON a.autorid = o.korisnikId Where eksperimentid = ?'
   komentari = query_db(get_comments_sql, expr_id)

   return render_template('experiment.html',
         uid=expr_id,
         editable=is_author,
         naziv=naziv, vrijemeod=vrijemeod, vrijemedo=vrijemedo,
         verificiran=verificiran,
         eid=eid, razvojnoOkruzenje=razvojnoOkruzenje,
         alat=alat, platforma=platforma,
         prosjek=prosjek, komentari=komentari,
         autori=autori,
         rezultat=rezultati,
         ocjene=ocjene
   )


@app.route('/edit/experiment/<uid>', methods=['GET'])
@login_required
def edit_experiment(uid):
   author_id = current_authorid()
   authors = get_authors(uid)

   if author_id not in authors:
      return 'Restricted'

   platforme, razvojnaOkruzenja, alati = get_elements()

   experiment = get_experiment(uid)
   uid, naziv, vrijemeod, vrijemedo = experiment[:4]

   platforma = get_experiment_platform(uid)
   platformaid = platforma[0] if platforma else 0

   alat = get_experiment_tool(uid)
   alatid = alat[0] if alat else 0

   razvojnoOkruzenje = get_experiment_env(uid)
   razvojnoOkruzenjeId = razvojnoOkruzenje[0] if razvojnoOkruzenje else 0

   # autori
   author_ids = get_authors(uid)

   sql = '''
         select autorid, (ime || ' ' || prezime) AS ime from Autor
         where ''' + ' or '.join(['autorid = ?'] * len(author_ids))

   dodaniautori = query_db(sql, *author_ids)

   autori = query_db('SELECT autorid, (ime || \' \' || prezime) AS ime FROM Autor')
   autori = [a for a in autori if a not in dodaniautori]

   rezultati = get_results(uid)

   sql = '''
         update eksperiment set verificiran = 0
         where eksperimentid = ?'''

   update_db(sql, uid)

   return render_template('edit_experiment.html',
         uid=uid,
         platformaid=platformaid,
         alatid=alatid,
         razvojnoOkruzenjeId=razvojnoOkruzenjeId,
         platforme=platforme,
         razvojnaOkruzenja=razvojnaOkruzenja,
         alati=alati,
         naziv=naziv,
         vrijemeod=vrijemeod,
         vrijemedo=vrijemedo,
         dodaniautori=dodaniautori,
         autori=autori,
         rezultati=rezultati
   )


@app.route('/dettach_author/<experimentid>/<autorid>', methods=['GET'])
@login_required
def dettach_author(experimentid, autorid):
   dettach_author_db(autorid=autorid, eksperimentid=experimentid)
   return redirect(url_for('edit_experiment', uid=experimentid))


@app.route('/dettach_author_work/<workid>/<autorid>', methods=['GET'])
@login_required
def dettach_author_work_db(workid, autorid):
   sql = '''
         DELETE FROM AutoriranjeZnanstvenogRada
         WHERE autorId = ? AND znanstveniRadId = ?
         '''
         
   update_db(sql, autorid, workid)
   return redirect(url_for('edit_work', uid=workid))


def build_update_query(table_name, uid_name, uid, fields):
   updateFields = ["%s = ?" % k for k, v in fields.iteritems()]
   sql = 'update %s SET %s WHERE %s = ?' % (table_name, ', '.join(updateFields), uid_name)
   args = fields.values()
   args.append(uid)
   return update_db(sql, *args)


def build_insert_query(table_name, fields):
   sql = 'INSERT INTO %s (%s) VALUES (%s);' % (table_name, ', '.join(fields.keys()), ', '.join(['?']*len(fields)))
   args = fields.values()
   return update_db(sql, *args)


def attach_author_db(autorid, eksperimentid):
   return build_insert_query(table_name='AutoriranjeEksperimenta', fields={'autorId': autorid, 'eksperimentId': eksperimentid})


def dettach_author_db(autorid, eksperimentid):
   sql = 'DELETE FROM AutoriranjeEksperimenta WHERE autorId = ? AND eksperimentId = ?'
   return update_db(sql, autorid, eksperimentid)


@app.route('/edit/experiment/<uid>', methods=['POST'])
@login_required
def update_experiment(uid):
   allowed = ['naziv', 'vrijemeod', 'vrijemedo', 'platformaid', 'razvojnoOkruzenjeid', 'alatid']
   data = {k: v for k, v in request.form.iteritems() if k in allowed}
   success = build_update_query(table_name='Eksperiment', uid_name='eksperimentId', uid=uid, fields=data)

   # dodavanje novog autora
   if 'dodajautora' in request.form:
      attach_author_db(autorid=request.form['autorid'], eksperimentid=uid)

   # dodavanje novog rezultat
   if 'dodajrezultat' in request.form:
      add_result(experimentid=uid,
         name=request.form['imeRezultata'],
         value=request.form['vrijednost'],
         unit=request.form['jedinica']
      )

   return redirect(url_for('edit_experiment', uid=uid))

@app.route('/delete_result/<resultid>')
def delete_result(resultid):
   sql = 'DELETE FROM Rezultat WHERE RezultatId = ?'
   update_db(sql, resultid)
   return redirect(request.args['redirect_uri'] if 'redirect_uri' in request.args else url_for('index'))


@app.route('/work/add', methods=['GET', 'POST'])
@login_required
def add_work():
   if request.method == 'POST':
      naziv = request.form['naslov']
      sazetak = request.form['sazetak']

      sql = '''
            insert into ZnanstveniRad
            (naslov, sazetak)
            values (?, ?)'''

      uid = update_db(sql, naziv, sazetak)
      attach_author_work_db(current_authorid(), uid)

      return redirect(url_for('edit_work', uid=uid))

   return render_template('add_work.html')


def get_work_authors(uid): 
   author_id = current_authorid()

   sql = '''
         select autorid from AutoriranjeZnanstvenogRada
         where znanstveniRadid = ?
         '''

   autori = query_db(sql, uid)
   return [x for x, in autori]


@app.route('/work/edit/<uid>/', methods=['GET'])
@login_required
def edit_work(uid):
   sql = 'SELECT * FROM ZnanstveniRad WHERE znanstveniradid = ?'
   uid, naslov, sazetak, verificiran, znanstveniSkupId, casopisId = query_db(sql, uid)[0]

   casopisi = query_db('SELECT * FROM Casopis')
   skupovi = query_db('SELECT * FROM ZnanstveniSkup')

   author_ids = get_work_authors(uid)

   sql = '''
         select autorid, (ime || ' ' || prezime) as ime from Autor
         where ''' + ' or '.join(['autorid = ?'] * len(author_ids))

   dodaniautori = query_db(sql, *author_ids)

   autori = query_db("select autorid, (ime || ' ' || prezime) as ime from Autor")
   autori = [a for a in autori if a not in dodaniautori]

   return render_template('edit_work.html',
      naslov=naslov,
      sazetak=sazetak,
      verificiran=verificiran,
      znanstveniSkupId=znanstveniSkupId,
      casopisId=casopisId,
      casopisi=casopisi,
      uid=uid,
      autori=autori,
      dodaniautori=dodaniautori,
      skupovi=skupovi
   )


def attach_author_work_db(authorId, workId):
   return build_insert_query(
         table_name='AutoriranjeZnanstvenogRada',
         fields={'autorId': authorId, 'znanstveniRadId': workId}
   )

@app.route('/work/edit/<uid>/', methods=['POST'])
def update_work(uid):
   allowed = ['naziv', 'sazetak']

   data = {k: v for k, v in request.form.iteritems() if k in allowed}
   
   success = build_update_query(
         table_name='ZnanstveniRad',
         uid_name='znanstveniRadId',
         uid=uid,
         fields=data
   )

   File = request.files['datoteka']

   if File:
      File.save(os.path.join(app.config['UPLOAD_FOLDER'], 'work{0}.txt'.format(uid)))

   if 'dodajautora' in request.form:
      attach_author_work_db(authorId=request.form['autorid'], workId=uid)

   return redirect(url_for('edit_work', uid=uid))


def hard_code():
   sql = '''
         insert into Eksperiment
         (naziv, vrijemeod, vrijemedo, razvojnoOkruzenjeId, alatId,
          platformaId)
         values (?, ?, ?, ?, ?, ?)
         '''
   uid = update_db(sql, 
         "Test1337", "26.03.2012. 08:12",
         "17.07.2013. 22:08", 1, 1, 1)
         # RazOk: Eclipse, Plat: Rasp sustav, Alat: Vim  

   sql = '''
         insert into AutoriranjeEksperimenta
         (eksperimentId, autorId)
         values (?, ?)
         '''

   update_db(sql, uid, 4)

   for i in xrange(3):
      name = 'val%d' % i
      sql = '''
            insert into Rezultat
            (imeRezultata, vrijednost, jedinica, eksperimentId)
            values (?, ?, ?, ?)
            '''
            
      update_db(sql, name, i + 1, 'K', uid)

   return redirect(url_for('edit_experiment', uid=uid))


@app.route('/experiment/add/', methods=['GET', 'POST'])
@login_required
def add_experiment():
   if request.method == 'POST':
      if request.files:
         return hard_code()

      naziv = request.form['naziv']
      vrijemeod = request.form['vrijemeod']
      vrijemedo = request.form['vrijemedo']

      sql = '''
            insert into Eksperiment
            (naziv, vrijemeod, vrijemedo)
            values (?, ?, ?)
            '''

      update_db(sql, naziv, vrijemeod, vrijemedo)

      sql = '''
            select max(eksperimentId), naziv from Eksperiment
            where naziv = ? '''
      uid, naziv = query_db(sql, naziv)[0]

      attach_author_db(eksperimentid=uid, autorid=current_authorid())

      return redirect(url_for('edit_experiment', uid=uid))

   platforme, razvojnaOkruzenja, alati = get_elements()

   return render_template('add_experiment.html',
         platforme=platforme,
         razvojnaOkruzenja=razvojnaOkruzenja,
         alati=alati
   )


@app.route('/experiment/<experimentid>/comment', methods=['POST'])
def comment_experiment(experimentid):
   data = { 'korisnikId': current_authorid(), 'eksperimentId': experimentid, 'vrijednost': request.form['vrijednost'], 'komentar': request.form['komentar'] }
   build_insert_query('Ocjena', data)
   flash('Komentar spremljen!')
   return redirect(url_for('show_experiment', expr_id=experimentid))


@app.route('/add/<tip>', methods=['GET'])
@login_required
def add_element(tip):
   if tip == 'work':
      return render_template('add_work.html')
   else:
      akuzativ = {'platform': 'Platformu',
                  'razvojnookruzenje': 'Razvojno okruzenje',
                  'alat': 'Alat'}
      return render_template('add_tip.html', tip_ak=akuzativ[tip], tip=tip, 
            redirect_uri=(request.args['redirect_uri'] if 'redirect_uri' in request.args else url_for('index')))


@app.route('/add/<tip>', methods=['POST'])
@login_required
def insert_element(tip):
   tables = {'platform': 'Platforma', 'razvojnookruzenje': 'RazvojnoOkruzenje', 'alat': 'Alat'}
   allowed = ['naziv', 'skraceninaziv', 'inacica', 'webstranica', 'tip', 'cijena']
   fields = {k: v for (k, v) in request.form.iteritems() if k in allowed}

   success = build_insert_query(table_name=tables[tip], fields=fields)

   return redirect(request.args['redirect_uri'] if 'redirect_uri' in request.args else url_for('index'))


@app.route('/profil')
@login_required
def profile():
   autorId = current_authorid()

   sql = '''
         select e.eksperimentid, naziv from autoriranjeeksperimenta as ae
         join eksperiment as e on e.eksperimentid = ae.eksperimentid
         where autorid = ?
         '''

   experiments = query_db(sql, autorId)

   sql = '''
         select z.znanstveniRadId, naslov from AutoriranjeZnanstvenogRada as az
         join ZnanstveniRad as z on z.ZnanstveniRadid = az.ZnanstveniRadid
         where autorId = ?
         '''

   works = query_db(sql, autorId)

   sql = '''
         select rokUplate from Korisnik
         where korisnikId = ?
         '''

   exp_date, = query_db(sql, current_user.userid)[0]
   if exp_date:
      exp_dt = datetime.datetime.strptime(exp_date, '%d.%m.%Y.')
      paid = exp_dt > datetime.datetime.today()
   else:
      paid = False

   return render_template('profil.html',
         user=current_user,
         experiments=experiments,
         works=works,
         exp_date=exp_date,
         paid=paid)


def dohvati_prigovore():
   sql = "SELECT p.*, (a.ime || ' ' || a.prezime) as autor FROM Prigovor p JOIN Autor a ON a.autorid = p.autorid ORDER BY p.prigovorid DESC"
   prigovori = query_db(sql)

   for i, p in enumerate(prigovori):
      pid, tip, targetid = p[:3]
      if tip == 'experiment':
         table, fetchColumn, conditionColumn = 'Eksperiment', 'naziv', 'eksperimentid'
      else:
         table, fetchColumn, conditionColumn = 'ZnanstveniRad', 'naslov', 'znanstveniradid'
      sql = 'SELECT %s FROM %s WHERE %s = ?' % (fetchColumn, table, conditionColumn)
      row = query_db(sql, pid)[0]
      prigovori[i] = p + (row[0],)

   return prigovori


@app.route('/experiment/<uid>/prigovori', methods=['POST'])
@login_required
def prigovori(uid):
   flash('Prigovor spremljen!')
   data = { 'targetid': 'uid', 'tip': request.form['tip'],
      'autorid': current_authorid(),
      'tekst': request.form['tekst']
   }
   build_insert_query('Prigovor', data)

   return redirect(url_for('show_experiment', expr_id=uid))


@app.route('/prigovori')
@login_required
def svi_prigovori():
   prigovori = dohvati_prigovore()
   return render_template('svi_prigovori.html',
      prigovori=prigovori
   )


@app.route('/prihvati_prigovor/<uid>')
@login_required
def prihvati_prigovor(uid):
   sql = 'select targetid, autorid from Prigovor where prigovorId = ?'
   targetid, autorid = query_db(sql, uid)[0]

   sql = '''
         update eksperiment set verificiran = 0
         where eksperimentid = ?'''
   update_db(sql, uid)

   sql = 'delete from Prigovor where prigovorId = ?' 
   update_db(sql, uid)

   return redirect(url_for('svi_prigovori'))


@app.route('/odbaci_prigovor/<uid>')
@login_required
def odbaci_prigovor(uid):
   sql = 'delete from Prigovor where prigovorId = ?'
   update_db(sql, uid)
   return redirect(url_for('svi_prigovori'))


@app.route('/sve_promjene')
@login_required
def sve_promjene():
   sql = '''
         select eksperimentId, naziv from Eksperiment
         where verificiran = 0
         '''

   experiments = query_db(sql)

   sql = '''
         select znanstveniRadId, naslov from ZnanstveniRad
         where verificiran = 0
         '''

   works = query_db(sql)

   return render_template('sve_promjene.html',
         experiments=experiments,
         works=works)


@app.route('/paid')
@login_required
def paid():
   return 'Poslan upit'


@app.route('/preuzmi_rezultate/<experimentid>')
@login_required
def preuzmi_rezultate(experimentid):
   sql = '''
         select imeRezultata, vrijednost, jedinica from Rezultat
         where eksperimentId=?
         '''
   results = query_db(sql, experimentid)

   resp  = ','.join(["%s(%s)" % (i, j) for i, _, j in results]) + '\n'
   resp += ','.join([str(x) for _, x, _ in results]) + '\n'

   response = make_response(resp)
   response.headers['Content-Disposition'] = 'attachment; filename=results.csv'

   return response


@app.route('/izvjesca', methods=['POST', 'GET'])
@login_required
def izvjesca():
   if request.method == 'POST':
      if 'aut' in request.form:
         autid = request.form['autorid']
         
         sql = '''
               select count(*) from AutoriranjeEksperimenta
               where autorId = ? 
               '''
         cnt, = query_db(sql, autid)[0]

         sql = "select (ime || ' ' || prezime) from Autor where autorId = ?"
         ime, = query_db(sql, autid)[0]


         resp = 'Autor,Broj\n%s,%d' % (ime, cnt)

         response = make_response(resp)
         response.headers['Content-Disposition'] = 'attachment; filename=izv.csv'
         return response

      if 'ocj' in request.form:
         ocjena = request.form['ocjena']
         
         try:
            ocjena = float(ocjena)
            if ocjena < 1 or ocjena > 10: 
               raise Error('Krivo upisana ocjena')
            
            sql = '''
                  select count(*) from Eksperiment as e
                  where 
                     (select avg(vrijednost) from ocjena as o
                      where o.eksperimentId = e.eksperimentId
                     ) = ?
                  '''

            cnt, = query_db(sql, ocjena)[0]

            resp = 'Ocjena,Broj\n%f,%d' % (ocjena, cnt)
            response = make_response(resp)
            response.headers['Content-Disposition'] = 'attachment; filename=izv.csv'
            return response
         except Exception, e:
            print e
            flash('Krivo upisana ocjena')


   sql = "select autorId, (ime || ' ' || prezime) from Autor"
   authors = query_db(sql)

   return render_template('izvjesca.html',
         authors=authors)


@app.route('/prihvati_registraciju/<uid>')
@login_required
def prihvati_registraciju(uid):
   sql = '''
         update Korisnik set aktivan = 1
         where korisnikId = ?
         '''

   update_db(sql, uid)

   return redirect(url_for('sve_registracije'))


@app.route('/odbaci_registraciju/<uid>')
@login_required
def odbaci_registraciju(uid):
   return redirect(url_for('sve_registracije'))


@app.route('/sve_registracije')
@login_required
def sve_registracije():
   sql = '''
         select korisnikId, username from Korisnik
         where aktivan = ?
         '''
   registracije = query_db(sql, 0)
   return render_template('sve_registracije.html', registracije=registracije)

if __name__ == '__main__':
   init_db()
   app.run(debug=True)

